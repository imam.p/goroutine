package mapper

import "goroutine/user"

func NewUser() []*user.User {

	mapCreate := []*user.User{
		{Name: "John", Status: "active", Settings: &user.Settings{NotificationsEnabled: true}},
		{Name: "Carl", Status: "deactive", Settings: &user.Settings{NotificationsEnabled: false}},
		{Name: "Paul", Status: "deactive", Settings: &user.Settings{NotificationsEnabled: true}},
		{Name: "Sam", Status: "deactive", Settings: &user.Settings{NotificationsEnabled: true}},
	}
	return mapCreate
}

func ExistingUsers() []*user.User {
	mapExist := []*user.User{
		{Name: "Jessica", Status: "deactive", Settings: &user.Settings{NotificationsEnabled: true}},
		{Name: "Eric", Status: "active", Settings: &user.Settings{NotificationsEnabled: true}},
		{Name: "Laura", Status: "active", Settings: &user.Settings{NotificationsEnabled: true}},
	}
	return mapExist
}
