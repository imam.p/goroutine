package user

type User struct {
	Id       int
	Name     string
	LastName string
	Status   string
	Tags     []*Tag
	Settings *Settings
}
