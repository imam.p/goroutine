package service

import (
	"fmt"
	"goroutine/user"
	"time"
)

type NotificationsService struct {
}

func FilterNewUsersByStatus(usersToUpdate chan<- []*user.User, users []*user.User) {
	defer close(usersToUpdate)
	filteredUsers := []*user.User{}
	for _, user := range users {
		if user.Status == "active" && user.Settings.NotificationsEnabled {
			filteredUsers = append(filteredUsers, user)
			fmt.Println("filternya [][][]", user.Name)
		}
	}
	usersToUpdate <- filteredUsers
}

func UpdateUsers(usersToUpdate <-chan []*user.User, userToNotify chan<- *user.User, users []*user.User) {
	defer close(userToNotify)
	for _, usr := range users {
		usr.Tags = append(usr.Tags, &user.Tag{Name: "Test", Type: "test"})
		fmt.Println("========================[][][]")
		fmt.Println("new user nya [][][]", usr)
	}

	newUsers := <-usersToUpdate

	for _, usr := range newUsers {
		time.Sleep(1 * time.Second)
		usr.Tags = append(usr.Tags, &user.Tag{Name: "Test", Type: "test"})
		userToNotify <- usr
		fmt.Println("============================")
		fmt.Println("User Notifinya  [][][]", usr)
	}
}

func NotifyUsers(userToNotify <-chan *user.User, users []*user.User) {

	service := &NotificationsService{}
	for _, user := range users {
		service.SendEmailNotification(user, "Tags", "A new tag has been added to your profile!!")
	}

	for user := range userToNotify {
		service.SendEmailNotification(user, "Tags", "You got your first tag!!")
	}
}

func (n *NotificationsService) SendEmailNotification(user *user.User, title, message string) {
	fmt.Printf("Email Notification Sent to %v, Hi %s, %s\n", user, user.Name, message)
}
