package main

import (
	"goroutine/mapper"
	"goroutine/service"
	"goroutine/user"
	"time"
)

type Tag struct {
	Name string
	Type string
}

type NotificationsService struct {
}

func main() {
	usersToUpdate := make(chan []*user.User)
	userToNotify := make(chan *user.User)
	newUsers := mapper.NewUser()
	existingUsers := mapper.ExistingUsers()

	go service.FilterNewUsersByStatus(usersToUpdate, newUsers)
	// go filterNewUsersByStatus(usersToUpdate, newUsers)
	time.Sleep(1 * time.Second)
	go service.UpdateUsers(usersToUpdate, userToNotify, existingUsers)
	service.NotifyUsers(userToNotify, existingUsers)
}

// func filterNewUsersByStatus(usersToUpdate chan<- []*user.User, users []*user.User) {
// 	defer close(usersToUpdate)
// 	filteredUsers := []*user.User{}
// 	for _, user := range users {
// 		if user.Status == "active" && user.Settings.NotificationsEnabled {
// 			filteredUsers = append(filteredUsers, user)
// 			fmt.Println("filternya [][][]", user.Name)
// 		}
// 	}
// 	usersToUpdate <- filteredUsers
// }

// func updateUsers(usersToUpdate <-chan []*user.User, userToNotify chan<- *user.User, users []*user.User) {
// 	defer close(userToNotify)
// 	for _, usr := range users {
// 		usr.Tags = append(usr.Tags, &user.Tag{Name: "Test", Type: "test"})
// 		fmt.Println("========================[][][]")
// 		fmt.Println("new user nya [][][]", usr)
// 	}

// 	newUsers := <-usersToUpdate

// 	for _, usr := range newUsers {
// 		time.Sleep(1 * time.Second)
// 		usr.Tags = append(usr.Tags, &user.Tag{Name: "Test", Type: "test"})
// 		userToNotify <- usr
// 		fmt.Println("============================")
// 		fmt.Println("User Notifinya  [][][]", usr)
// 	}
// }

// func notifyUsers(userToNotify <-chan *user.User, users []*user.User) {

// 	service := &NotificationsService{}
// 	for _, user := range users {
// 		service.SendEmailNotification(user, "Tags", "A new tag has been added to your profile!!")
// 	}

// 	for user := range userToNotify {
// 		service.SendEmailNotification(user, "Tags", "You got your first tag!!")
// 	}
// }

// func (n *NotificationsService) SendEmailNotification(user *user.User, title, message string) {
// 	fmt.Printf("Email Notification Sent to %v, Hi %s, %s\n", user, user.Name, message)
// }
